---
layout: page
title: Our Mission
---

The Buffalo Niagara Coalition for Open Government is a nonpartisan organization comprised of journalists, activists, attorneys, educators, news media organizations, and other concerned citizens who value open government and demand freedom of information.