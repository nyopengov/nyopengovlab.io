---
layout: page
title: Resources
description: Helpful tools and resources to fight for open government.
---

## Links

<a href="http://www.dos.ny.gov/coog/" target="_blank">NY State Committee on Open Government</a>

<a href="http://www.dos.ny.gov/video/coog.html" target="_blank">NSYCOG Video Explanations</a>

<a href="http://www.dos.ny.gov/coog/Right_to_know.html" target="_blank">NYSCOG Your Right to Know</a>
