---
layout: post
title: Public Disciplinary Records
date: 2016-09-21 00:00:00 +0000
---

Robert Freeman the Executive Director of the New York State Committee on Open Government recently wrote an interesting article about access to public employee disciplinary records.

In New York, the disciplinary records of police officers and correctional officers are treated differently than other public employees.

Freeman makes some good points as to why police and correctional officer records should be open to the public.

[View Article](http://www.nydailynews.com/new-york/expert-fix-discipline-records-law-hold-police-accountable-article-1.2764584)